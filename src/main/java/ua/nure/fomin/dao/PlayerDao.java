package ua.nure.fomin.dao;

import ua.nure.fomin.entities.Player;

import java.util.List;


public interface PlayerDao {

    void insert(Player player);

    void update(Player player);

    void delete(List<Integer> identifiers);

    List<Player> read();

    Player find(int id);

}