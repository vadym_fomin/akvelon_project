package ua.nure.fomin.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.nure.fomin.entities.Player;
import ua.nure.fomin.entities.Position;
import ua.nure.fomin.services.PlayerService;

import java.util.List;


@Controller
@RequestMapping("/players")
public class PlayerController {

    private static final String VIEW_NAME = "edit";

    private static final String PLAYER_FORM_ATTRIBUTE = "playerForm";

    private static final String POSITIONS_ATTRIBUTE = "positions";

    private static final String REDIRECT = "redirect:/home";

    @Autowired
    private PlayerService playerService;

    public PlayerController() {
    }

    @RequestMapping(value = "/player/{id}", method = RequestMethod.GET)
    public String showEditingFieldPage(@PathVariable Integer id, Model model) {
        model.addAttribute(POSITIONS_ATTRIBUTE, Position.values());
        if (id == 0) {
            model.addAttribute(PLAYER_FORM_ATTRIBUTE, new Player());
        } else {
            Player player = playerService.find(id);
            model.addAttribute(PLAYER_FORM_ATTRIBUTE, player);
        }
        return VIEW_NAME;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Player> getPlayers() {
        return playerService.read();
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addPlayer(@ModelAttribute(PLAYER_FORM_ATTRIBUTE) Player player) {
        playerService.insert(player);
        return REDIRECT;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String editPlayer(@ModelAttribute(PLAYER_FORM_ATTRIBUTE) Player player) {
        playerService.update(player);
        return REDIRECT;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String deletePlayer(@RequestBody List<Integer> playersId) {
        playerService.delete(playersId);
        return REDIRECT;
    }

}
