package ua.nure.fomin.dao;

import org.springframework.stereotype.Repository;
import ua.nure.fomin.entities.Player;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


@Repository
public class PlayerDaoImpl implements PlayerDao {

    private static final String GET_ALL_PLAYERS = "select p from Player p";

    @PersistenceContext
    private EntityManager em;

    public PlayerDaoImpl() {
    }

    @Override
    public void insert(Player player) {
        em.persist(player);
    }

    @Override
    public void update(Player player) {
        em.merge(player);

    }

    @Override
    public void delete(List<Integer> identifiers) {
        for (Integer id : identifiers) {
            em.remove(find(id));
        }

    }

    @Override
    public List<Player> read() {
        return em.createQuery(GET_ALL_PLAYERS).getResultList();
    }

    @Override
    public Player find(int id) {
        return em.find(Player.class, id);
    }
}
