<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <title>players</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <spring:url value="/resources/css/style.css" var="customCss"/>
    <spring:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" var="bootstrapCss"/>
    <spring:url value="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" var="jquery"/>
    <spring:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" var="bootstrapJs"/>
    <spring:url value="players/delete" var="delete"/>
    <spring:url value="/resources/js/refreshListener.js" var="refreshListener"/>
    <spring:url value="/resources/js/deleteListener.js" var="deleteListener"/>
    <spring:url value="/resources/js/editListener.js" var="editListener"/>
    <spring:url value="/resources/js/functions.js" var="functions"/>

    <link rel="stylesheet" href="${bootstrapCss}">
    <link rel="stylesheet"  href="${customCss}"/>
    <script src="${jquery}"></script>
    <script src="${bootstrapJs}"></script>
    <script src="${refreshListener}"></script>
    <script src="${deleteListener}"></script>
    <script src="${editListener}"></script>
    <script src="${functions}"></script>
</head>
