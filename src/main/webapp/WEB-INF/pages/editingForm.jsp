<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<spring:url value="/players/edit" var="edit"/>

<form:form method="post" action="${edit}" modelAttribute="playerForm">
    <div class="form-group">
        <form:input type="hidden" path="id" value="${playerForm.id}"/>
        <label for="name">Name:</label>
        <form:input type="text" class="form-control" id="name" path="name" value="${playerForm.name}"/>
    </div>
    <div class="form-group">
        <label for="age">Age:</label>
        <form:input type="number" class="form-control" id="age" path="age" value="${playerForm.age}"/>
    </div>
    <div class="form-group">
        <label for="country">Country:</label>
        <form:input type="text" class="form-control" id="country" path="country" value="${playerForm.country}"/>
    </div>
    <div class="form-group">
        <label for="position">Name:</label>
        <form:select id="position" class="form-control" path="position">
            <c:forEach var="position" items="${positions}">
                <c:choose>
                    <c:when test="${position eq playerForm.position}">
                        <form:option value="${position}" selected="true">${position}</form:option>
                    </c:when>
                    <c:otherwise>
                        <form:option value="${position}">${position}</form:option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </form:select>
    </div>
    <button type="submit" class="btn btn-default">
        <span class="glyphicon glyphicon-pencil"></span> Edit
    </button>
    <button type="button" id="cancel" class="btn btn-default">
        <span class="glyphicon glyphicon-remove-circle"></span> Cancel
    </button>
</form:form>