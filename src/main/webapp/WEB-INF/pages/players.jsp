<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<html lang="en">
<tiles:insertAttribute name="header"/>
<body>

<div class="container">
    <h2>All players</h2>
    <form method="post" action="${delete}" id="playersForm">
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th>name</th>
                <th>age</th>
                <th>country</th>
                <th>position</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="player" items="${players}">
                <tr>
                    <td>${player.name}</td>
                    <td>${player.age}</td>
                    <td>${player.country}</td>
                    <td>${player.position}</td>
                    <td><input type="checkbox" id="playersId" value="${player.id}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <button type="button" id="refresh" class="btn btn-default">
            <span class="glyphicon glyphicon-refresh"></span> Refresh
        </button>
        <button type="button" id="edit" class="btn btn-default">
            <span class="glyphicon glyphicon-plus"></span> Edit
        </button>
        <button type="submit" id="delete" class="btn btn-default">
            <span class="glyphicon glyphicon-minus"></span> Delete
        </button>
    </form>
</div>

<tiles:insertAttribute name="error"/>

</body>
</html>
