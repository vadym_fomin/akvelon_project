import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import ua.nure.fomin.controllers.PlayerController;
import ua.nure.fomin.entities.Player;
import ua.nure.fomin.services.PlayerService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlayerControllerTest {

    private static final String VIEW_NAME = "edit";

    private static final String PLAYER_FORM_ATTRIBUTE = "playerForm";

    private static final String REDIRECT = "redirect:/home";

    @Mock
    private PlayerService playerService;

    @InjectMocks
    private PlayerController playerController;

    @Test
    public void showEditingFieldPageTest() {
        Model model = spy(Model.class);
        Player player = new Player();
        player.setId(1);
        when(playerService.find(anyInt())).thenReturn(player);

        assertEquals(playerController.showEditingFieldPage(0, model), VIEW_NAME);
        verify(model).addAttribute(PLAYER_FORM_ATTRIBUTE, new Player());

        assertEquals(playerController.showEditingFieldPage(1, model), VIEW_NAME);
        verify(playerService).find(1);
        verify(model).addAttribute(PLAYER_FORM_ATTRIBUTE, player);
    }

    @Test
    public void getPlayersTest() {
        List<Player> playerList = new ArrayList<>();
        when(playerService.read()).thenReturn(playerList);
        assertEquals(playerController.getPlayers(), playerList);
        verify(playerService).read();
    }

    @Test
    public void addPlayerTest() {
        Player player = new Player();
        assertEquals(playerController.addPlayer(player), REDIRECT);
        verify(playerService).insert(player);
    }

    @Test
    public void editPlayerTest() {
        Player player = new Player();
        assertEquals(playerController.editPlayer(player), REDIRECT);
        verify(playerService).update(player);
    }

    @Test
    public void deletePlayerTest() {
        List<Integer> identifiers = new ArrayList<>();
        assertEquals(playerController.deletePlayer(identifiers), REDIRECT);
        verify(playerService).delete(identifiers);
    }
}
