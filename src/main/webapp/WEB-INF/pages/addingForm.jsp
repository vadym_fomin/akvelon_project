<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<spring:url value="/players/add" var="add"/>

<form:form method="post" action="${add}" modelAttribute="playerForm">
    <div class="form-group">
        <label for="name">Name:</label>
        <form:input type="text" class="form-control" id="name" placeholder="Enter name" path="name"/>
    </div>
    <div class="form-group">
        <label for="age">Age:</label>
        <form:input type="number" class="form-control" id="age" placeholder="Enter age" path="age"/>
    </div>
    <div class="form-group">
        <label for="country">Country:</label>
        <form:input type="text" class="form-control" id="country" placeholder="Enter country" path="country"/>
    </div>
    <div class="form-group">
        <label for="position">Position:</label>
        <form:select id="position" class="form-control" path="position">
            <c:forEach var="position" items="${positions}">
                <form:option value="${position}">${position}</form:option>
            </c:forEach>
        </form:select>
    </div>
    <button type="submit" class="btn btn-default">
        <span class="glyphicon glyphicon-plus"></span> Add
    </button>
    <button type="button" id="cancel" class="btn btn-default">
        <span class="glyphicon glyphicon-remove-circle"></span> Cancel
    </button>
</form:form>

