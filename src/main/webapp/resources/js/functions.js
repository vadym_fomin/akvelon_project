function refresh(players) {
    var table = document.getElementsByTagName("table")[0];
    $("tbody").remove();
    var body = document.createElement("tbody");

    for (var i = 0; i < players.length; i++) {
        var row = document.createElement("tr");
        var name = document.createElement("td");
        name.appendChild(document.createTextNode(players[i].name));
        var age = document.createElement("td");
        age.appendChild(document.createTextNode(players[i].age));
        var country = document.createElement("td");
        country.appendChild(document.createTextNode(players[i].country));
        var position = document.createElement("td");
        position.appendChild(document.createTextNode(players[i].position));
        row.appendChild(name);
        row.appendChild(age);
        row.appendChild(country);
        row.appendChild(position);
        var input = "<td><input type='checkbox'  value="+ players[i].id +" /></td>";
        row.insertAdjacentHTML("beforeEnd", input);
        body.appendChild(row);
    }
    table.appendChild(body);
}

function getDataFromCheckboxes(){
    var boxes = $("input:checkbox");
    var data = new Array();
    var index = 0;
    for (var i = 0; i < boxes.length; i++) {
        var box = boxes[i];
        if ($(box).prop('checked')) {
            data[index] = $(box).val();
            index++;
        }
    }
    return data;
}

