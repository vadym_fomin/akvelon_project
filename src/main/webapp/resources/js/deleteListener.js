$(document).ready(function () {
    $("#playersForm").submit(function (event) {
        event.preventDefault();
        var data = getDataFromCheckboxes();
        $.ajax({
            type: 'POST',
            url: 'players/delete',
            async: true,
            data: JSON.stringify(data),
            contentType: "application/json",
            success: function () {
                $.ajax({
                    type: 'GET',
                    url: 'players',
                    dataType: 'json',
                    async: true,
                    success: function (players) {
                        refresh(players);
                    }
                });
            }
        });
    });
});

