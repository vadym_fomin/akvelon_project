<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<html lang="en">
<head>
    <title>Editing field</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <spring:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" var="bootstrapCss"/>
    <spring:url value="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" var="jquery"/>
    <spring:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" var="bootstrapJs"/>

    <link rel="stylesheet" href="${bootstrapCss}">
    <script src="${jquery}"></script>
    <script src="${bootstrapJs}"></script>
</head>
<body>

<div class="container">
    <h2>Save/add player</h2>
    <c:choose>
        <c:when test="${playerForm.id ne 0}">
            <tiles:insertAttribute name="editingForm"/>
        </c:when>
        <c:otherwise>
            <tiles:insertAttribute name="addingForm"/>
        </c:otherwise>
    </c:choose>
</div>
<script>
    $(document).ready(function () {
        $("#cancel").click(function () {
            window.location = '/home';
        });
    });
</script>
</body>
</html>
