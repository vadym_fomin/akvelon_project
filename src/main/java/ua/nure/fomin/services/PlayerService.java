package ua.nure.fomin.services;

import ua.nure.fomin.entities.Player;

import java.util.List;


public interface PlayerService {

    void insert(Player player);

    void update(Player player);

    void delete(List<Integer> identifiers);

    List<Player> read();

    Player find(int id);

}