package ua.nure.fomin.services;


import org.springframework.transaction.annotation.Transactional;
import ua.nure.fomin.dao.PlayerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.nure.fomin.entities.Player;

import java.util.List;

@Service
@Transactional
public class PlayerServiceImpl implements PlayerService {

    @Autowired
    private PlayerDao playerDao;

    public PlayerServiceImpl() {
    }

    @Override
    public void insert(Player player) {
        playerDao.insert(player);
    }

    @Override
    public void update(Player player) {
        playerDao.update(player);
    }

    @Override
    public void delete(List<Integer> identifiers) {
        playerDao.delete(identifiers);

    }

    @Override
    public List<Player> read() {
        return playerDao.read();
    }

    @Override
    public Player find(int id) {
        return playerDao.find(id);
    }
}
