package ua.nure.fomin.configs;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;


@Configuration
@EnableTransactionManagement
@PropertySource(value = {"classpath:user/password/host.properties"})
public class DbConfig implements EnvironmentAware {

    private Environment env;

    private static final String DRIVER = "jdbc.driverClassName";

    private static final String URL = "jdbc.url";

    private static final String USERNAME = "jdbc.username";

    private static final String PASSWORD = "jdbc.password";

    private static final String CONNECTION_POOL_INITIAL_SIZE = "dbcp.initialSize";

    private static final String CONNECTION_POOL_MAX_ACTIVE = "dbcp.maxActive";

    private static final String DIALECT = "hibernate.dialect";

    private static final String TABLE_CREATION = "hibernate.hbm2ddl.auto";

    private static final String SCRIPT = "javax.persistence.sql-load-script-source";

    private static final String PACKAGE = "ua.nure.fomin.entities";

    @Override
    public void setEnvironment(Environment env) {
        this.env = env;
    }

    @Bean
    public DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(env.getProperty(DRIVER));
        dataSource.setUrl(env.getProperty(URL));
        dataSource.setUsername(env.getProperty(USERNAME));
        dataSource.setPassword(env.getProperty(PASSWORD));
        dataSource.setInitialSize(Integer.parseInt(env.getProperty(CONNECTION_POOL_INITIAL_SIZE)));
        dataSource.setMaxActive(Integer.parseInt(env.getProperty(CONNECTION_POOL_MAX_ACTIVE)));
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource());
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setJpaProperties(getProperties());
        entityManagerFactoryBean.setPackagesToScan(PACKAGE);
        return entityManagerFactoryBean;
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

    @Bean
    public PersistenceAnnotationBeanPostProcessor persistenceAnnotationBeanPostProcessor() {
        return new PersistenceAnnotationBeanPostProcessor();
    }

    private Properties getProperties() {
        Properties properties = new Properties();
        properties.put(DIALECT, env.getProperty(DIALECT));
        properties.put(TABLE_CREATION, env.getProperty(TABLE_CREATION));
        properties.put(SCRIPT, env.getProperty(SCRIPT));
        return properties;
    }

}
