package ua.nure.fomin.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ua.nure.fomin.services.PlayerService;

@Controller
public class HomeController {

    private static final String VIEW_NAME = "home";

    private static final String PLAYERS_ATTRIBUTE = "players";

    @Autowired
    private PlayerService playerService;

    public HomeController() {
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String showHomePage(Model model) {
        model.addAttribute(PLAYERS_ATTRIBUTE, playerService.read());
        return VIEW_NAME;
    }
}
