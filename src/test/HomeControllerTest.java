import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import static org.mockito.Mockito.*;
import ua.nure.fomin.controllers.HomeController;
import ua.nure.fomin.entities.Player;
import ua.nure.fomin.services.PlayerService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class HomeControllerTest {

    private static final String VIEW_NAME = "home";

    private static final String PLAYERS_ATTRIBUTE = "players";

    @Mock
    private PlayerService playerService;

    @InjectMocks
    private HomeController homeController;

    @Test
    public void showHomePage(){
        Model model = spy(Model.class);
        List<Player> players = new ArrayList<>();
        when(playerService.read()).thenReturn(players);
        assertEquals(homeController.showHomePage(model),VIEW_NAME);
        verify(playerService).read();
        verify(model).addAttribute(PLAYERS_ATTRIBUTE,players);
    }
}
