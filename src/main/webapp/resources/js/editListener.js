$(document).ready(function () {
    $("#edit").click(function () {
        var data = getDataFromCheckboxes();
        var url = 'players/player/';
        if (data.length > 1) {
            $('.popup,.popup_overlay').fadeIn(400);
        } else {
            if (data.length == 0) {
                url = url + 0;
            }
            else {
                url = url + data[0];
            }
            window.location = url;
        }
    });
    $('.closer,.popup_overlay').click(function () {
        $('.popup,.popup_overlay').fadeOut(400);
    });
});
